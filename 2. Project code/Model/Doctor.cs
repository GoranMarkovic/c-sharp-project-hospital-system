﻿using ConsoleApp1.Repository;
using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1.Model
{
    class Doctor : User
    {
        private DoctorIdRepository doctorIdRepository;
        private DateTime startWorking;
        private DateTime endWorking;
        private String specialization;

        public Doctor()
        {
            doctorIdRepository = new DoctorIdRepository();
        }

        public DateTime StartWorking
        {
            get { return startWorking; }
            set
            {
                if (startWorking != value)
                {
                    startWorking = value;
                }
            }
        }

        public DateTime EndWorking
        {
            get { return endWorking; }
            set
            {
                if (endWorking != value)
                {
                    endWorking = value;
                }
            }
        }

        public String Specialization
        {
            get { return specialization; }
            set
            {
                if (specialization != value)
                {
                    specialization = value;
                }
            }
        }

        public int doctorIdSetter()
        {
            List<int> idsList = new List<int>();
            idsList = doctorIdRepository.findAllDoctorId();
            if (idsList.Count == 0) // ukoliko je lista prazna prvi id je 0
            {
                doctorIdRepository.addDoctorId(0);
                return 0;
            }
            else
            {
                int id;
                Random random = new Random();
                id = random.Next(1, 100);
                while (idsList.Contains(id))
                {
                    id = random.Next(1, 100);
                }
                doctorIdRepository.addDoctorId(id);
                return id;
            }
        }

    }
}
