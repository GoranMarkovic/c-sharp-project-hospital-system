﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1.Model
{
    class ConsumableEquipment : Equipment
    {
        private String typeOfConsumable;

        public String TypeOfConsumable
        {
            get { return typeOfConsumable; }
            set
            {
                if (value != typeOfConsumable)
                {
                    typeOfConsumable = value;
                }
            }
        }
    }
}
