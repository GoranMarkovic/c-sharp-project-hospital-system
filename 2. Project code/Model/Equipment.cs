﻿using ConsoleApp1.Repository;
using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1.Model
{
    class Equipment
    {
        private ConsumableEquipmentIdRepository consumableEquipmentIdRepository;
        private int id;
        private String name;
        private int quantity;

        public Equipment()
        {
            consumableEquipmentIdRepository = new ConsumableEquipmentIdRepository();
        }

        public int Id
        {
            get { return id; }
            set
            {
                if (value != id)
                {
                    id = value;
                }
            }
        }

        public String Name
        {
            get { return name; }
            set
            {
                if (value != name)
                {
                    name = value;
                }
            }
        }

        public int Quantity
        {
            get { return quantity; }
            set
            {
                if (value != quantity)
                {
                    quantity = value;
                }
            }
        }

        public int equipmentIdSetter()
        {
            List<int> idsList = new List<int>();
            idsList = consumableEquipmentIdRepository.findAllConsumableId();
            if (idsList.Count == 0) // ukoliko je lista prazna prvi id je 0
            {
                consumableEquipmentIdRepository.addConsumableId(0);
                return 0;
            }
            else
            {
                int id;
                Random random = new Random();
                id = random.Next(1, 100);
                while (idsList.Contains(id))
                {
                    id = random.Next(1, 100);
                }
                consumableEquipmentIdRepository.addConsumableId(id);
                return id;
            }
        }
    }
}
