﻿using ConsoleApp1.Repository;
using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1.Model
{
    class Secretary : User
    {
        private SecretaryIdRepository secretaryIdRepository;

        public Secretary()
        {
            secretaryIdRepository = new SecretaryIdRepository();
        }

        public int secretaryIdSetter()
        {
            List<int> idsList = new List<int>();
            idsList = secretaryIdRepository.findAllSecretaryId();
            if (idsList.Count == 0) // ukoliko je lista prazna prvi id je 0
            {
                secretaryIdRepository.addSecretaryId(0);
                return 0;
            }
            else
            {
                int id;
                Random random = new Random();
                id = random.Next(1, 100);
                while (idsList.Contains(id))
                {
                    id = random.Next(1, 100);
                }
                secretaryIdRepository.addSecretaryId(id);
                return id;
            }
        }
    }
}
