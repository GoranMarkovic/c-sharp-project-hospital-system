﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1.Model
{
    class User
    {
        private String password;
        private int id;
        private String firstName;
        private String lastName;
        private String address;
        private String email;
        private long jmbg;
        private String birth;
        private int phone;
        private String username;

        public String Password
        {
            get { return password; }
            set
            {
                if (value != password)
                {
                    password = value;
                }
            }
        }

        public String Username
        {
            get { return username; }
            set
            {
                if (value != username)
                {
                    username = value;
                }
            }
        }

        public int Id
        {
            get { return id; }
            set
            {
                if (value != id)
                {
                    id = value;
                }
            }
        }

        public String FirstName
        {
            get { return firstName; }
            set
            {
                if (value != firstName)
                {
                    firstName = value;
                }
            }
        }
        public String LastName
        {
            get { return lastName; }
            set
            {
                if (value != lastName)
                {
                    lastName = value;
                }
            }
        }

    }
}
