﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace ConsoleApp1.Repository
{
    class ConsumableEquipmentIdRepository
    {
        public void addConsumableId(int consumableId)
        {
            using (var writer = new StreamWriter(@"C:/Users/38162/Desktop/HCI/Files/consumableId.csv", true))
            {
                string line = consumableId.ToString() + ";";
                writer.WriteLine(line);
            }
        }

        public List<int> findAllConsumableId()
        {
            var strLines = File.ReadLines(@"C:/Users/38162/Desktop/HCI/Files/consumableId.csv");
            List<int> lista = new List<int>();

            foreach (var line in strLines)
            {
                lista.Add(Int32.Parse(line.Split(";")[0]));
            }
            return lista;
        }

        public void RemoveConsumableId(int medicationId)
        {
            List<String> lines = new List<string>();
            string line;

            using (System.IO.StreamReader file = new System.IO.StreamReader(@"C:/Users/38162/Desktop/HCI/Files/consumableId.csv"))
            {
                while ((line = file.ReadLine()) != null)
                {
                    if (line.Split(';')[0] != medicationId.ToString())
                    {
                        lines.Add(line);
                    }
                }
            }

            using (System.IO.StreamWriter outfile = new System.IO.StreamWriter(@"C:/Users/38162/Desktop/HCI/Files/consumableId.csv"))
            {
                outfile.Write(String.Join(System.Environment.NewLine, lines.ToArray()));
            }
        }
    }
}
