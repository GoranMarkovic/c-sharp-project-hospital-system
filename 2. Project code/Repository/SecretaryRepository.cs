﻿using ConsoleApp1.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace ConsoleApp1.Repository
{
    class SecretaryRepository
    {
        private SecretaryIdRepository secretaryIdRepository;

        public SecretaryRepository()
        {
            secretaryIdRepository = new SecretaryIdRepository();
        }

        public void AddSecretary(Model.Secretary secretary)
        {
            using (var writer = new StreamWriter(@"C:/Users/38162/Desktop/HCI/Files/secretaries.csv", true))
            {
                string[] values = { secretary.Id.ToString(), secretary.FirstName, secretary.LastName, secretary.Username, secretary.Password };
                string line = String.Join(";", values);
                writer.WriteLine(line);
            }
        }


        public void RemoveSecretary(int secretaryId)
        {
            secretaryIdRepository.RemoveSecretaryId(secretaryId);

            List<String> lines = new List<string>();
            string line;

            using (System.IO.StreamReader file = new System.IO.StreamReader(@"C:/Users/38162/Desktop/HCI/Files/secretaries.csv"))
            {
                while ((line = file.ReadLine()) != null)
                {
                    if (line.Split(';')[0] != secretaryId.ToString())
                    {
                        lines.Add(line);
                    }
                }
            }

            using (System.IO.StreamWriter outfile = new System.IO.StreamWriter(@"C:/Users/38162/Desktop/HCI/Files/secretaries.csv"))
            {
                outfile.Write(String.Join(System.Environment.NewLine, lines.ToArray()));
            }
        }


        public Model.Secretary FindSecretaryById(int secretaryId)
        {
            string line;
            Secretary secretary = new Secretary();
            using (System.IO.StreamReader file = new System.IO.StreamReader(@"C:/Users/38162/Desktop/HCI/Files/secretaries.csv"))
            {
                while ((line = file.ReadLine()) != null)
                {
                    if (line.Split(';')[0] == secretaryId.ToString())
                    {
                        secretary.Id = Int32.Parse(line.Split(';')[0]);
                        secretary.FirstName = line.Split(';')[1];
                        secretary.LastName = line.Split(';')[2];
                        secretary.Username = line.Split(';')[3];
                        secretary.Password = line.Split(';')[4];
                        break;
                    }
                }
            }
            return secretary;
        }



        public void UpdateSecretary(Secretary secretary)
        {
            List<String> lines = new List<string>();
            string line;

            using (System.IO.StreamReader file = new System.IO.StreamReader(@"C:/Users/38162/Desktop/HCI/Files/secretaries.csv"))
            {
                while ((line = file.ReadLine()) != null)
                {
                    if (line.Split(';')[0] == secretary.Id.ToString())
                    {
                        //line.Split(';')[1] = medication.Name;
                        //line.Split(';')[2] = medication.Ingridients;
                        //line.Split(';')[3] = medication.Uses;
                        //lines.Add(line);

                        string[] values = { secretary.Id.ToString(), secretary.FirstName, secretary.LastName, secretary.Username, secretary.Password };
                        string line1 = String.Join(";", values);
                        lines.Add(line1);
                    }
                    else
                    {
                        lines.Add(line);
                    }
                }
            }

            using (System.IO.StreamWriter outfile = new System.IO.StreamWriter(@"C:/Users/38162/Desktop/HCI/Files/secretaries.csv"))
            {
                outfile.Write(String.Join(System.Environment.NewLine, lines.ToArray()));
            }
        }

        public List<Secretary> FindAllSecretaries()
        {
            string line;
            List<Secretary> lista = new List<Secretary>();

            using (System.IO.StreamReader file = new System.IO.StreamReader(@"C:/Users/38162/Desktop/HCI/Files/secretaries.csv"))
            {
                while ((line = file.ReadLine()) != null)
                {
                    Secretary secretary = new Secretary();
                    secretary.Id = Int32.Parse(line.Split(';')[0]);
                    secretary.FirstName = line.Split(';')[1];
                    secretary.LastName = line.Split(';')[2];
                    secretary.Username = line.Split(';')[3];
                    secretary.Password = line.Split(';')[4];
                    lista.Add(secretary);
                }
            }

            return lista;
        }
    }
}
