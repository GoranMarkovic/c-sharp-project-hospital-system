// File:    MedicationRepository.cs
// Author:  38162
// Created: Friday, May 15, 2020 9:41:49 PM
// Purpose: Definition of Class MedicationRepository

using ConsoleApp1.Model;
using System;
using System.Collections.Generic;
using System.IO;

namespace ConsoleApp1.Repository
{
    public class MedicationRepository
    {
        private MedicationIdRepository medicationIdRepository;

        public MedicationRepository()
        {
            medicationIdRepository = new MedicationIdRepository();
        }

        public void AddMedication(Model.Medication medication)
        {
            using (var writer = new StreamWriter(@"C:/Users/38162/Desktop/HCI/Files/medications.csv", true))
            {
                string[] values = { medication.Id.ToString(), medication.Name, medication.Ingridients, medication.Uses };
                string line = String.Join(";", values);
                writer.WriteLine(line);
            }
        }


        public void RemoveMedication(int medicationId)
        {
            medicationIdRepository.RemoveMedicationId(medicationId);

            List<String> lines = new List<string>();
            string line;

            using (System.IO.StreamReader file = new System.IO.StreamReader(@"C:/Users/38162/Desktop/HCI/Files/medications.csv"))
            {
                while ((line = file.ReadLine()) != null)
                {
                    if (line.Split(';')[0] != medicationId.ToString())
                    {
                        lines.Add(line);
                    }
                }
            }

            using (System.IO.StreamWriter outfile = new System.IO.StreamWriter(@"C:/Users/38162/Desktop/HCI/Files/medications.csv"))
            {
                outfile.Write(String.Join(System.Environment.NewLine, lines.ToArray()));
            }
        }


        public Model.Medication FindMedicationById(int medicationId)
        {
            string line;
            Medication medication = new Medication();
            using (System.IO.StreamReader file = new System.IO.StreamReader(@"C:/Users/38162/Desktop/HCI/Files/medications.csv"))
            {
                while ((line = file.ReadLine()) != null)
                {
                    if (line.Split(';')[0] == medicationId.ToString())
                    {
                        medication.Id = Int32.Parse(line.Split(';')[0]);
                        medication.Name = line.Split(';')[1];
                        medication.Ingridients = line.Split(';')[2];
                        medication.Uses = line.Split(';')[3];
                        break;
                    }
                }
            }
            return medication;
        }



        public void UpdateMedication(Medication medication)
        {
            List<String> lines = new List<string>();
            string line;

            using (System.IO.StreamReader file = new System.IO.StreamReader(@"C:/Users/38162/Desktop/HCI/Files/medications.csv"))
            {
                while ((line = file.ReadLine()) != null)
                {
                    if (line.Split(';')[0] == medication.Id.ToString())
                    {
                        //line.Split(';')[1] = medication.Name;
                        //line.Split(';')[2] = medication.Ingridients;
                        //line.Split(';')[3] = medication.Uses;
                        //lines.Add(line);

                        string[] values = { medication.Id.ToString(), medication.Name, medication.Ingridients, medication.Uses };
                        string line1 = String.Join(";", values);
                        lines.Add(line1);
                    }
                    else
                    {
                        lines.Add(line);
                    }
                }
            }

            using (System.IO.StreamWriter outfile = new System.IO.StreamWriter(@"C:/Users/38162/Desktop/HCI/Files/medications.csv"))
            {
                outfile.Write(String.Join(System.Environment.NewLine, lines.ToArray()));
            }
        }

        public List<Medication> FindAllMedication()
        {
            string line;
            List<Medication> lista = new List<Medication>();
            
            using (System.IO.StreamReader file = new System.IO.StreamReader(@"C:/Users/38162/Desktop/HCI/Files/medications.csv"))
            {
                while ((line = file.ReadLine()) != null)
                {
                    Medication medication = new Medication();
                    medication.Id = Int32.Parse(line.Split(';')[0]);
                    medication.Name = line.Split(';')[1];
                    medication.Ingridients = line.Split(';')[2];
                    medication.Uses = line.Split(';')[3];
                    lista.Add(medication);
                }
            }
            
            return lista;
        }


    }
}

