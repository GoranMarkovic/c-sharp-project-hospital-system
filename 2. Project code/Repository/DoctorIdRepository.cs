﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace ConsoleApp1.Repository
{
    class DoctorIdRepository
    {
        public void addDoctorId(int doctorId)
        {
            using (var writer = new StreamWriter(@"C:/Users/38162/Desktop/HCI/Files/doctorId.csv", true))
            {
                string line = doctorId.ToString() + ";";
                writer.WriteLine(line);
            }
        }

        public List<int> findAllDoctorId()
        {
            var strLines = File.ReadLines(@"C:/Users/38162/Desktop/HCI/Files/doctorId.csv");
            List<int> lista = new List<int>();

            foreach (var line in strLines)
            {
                lista.Add(Int32.Parse(line.Split(";")[0]));
            }
            return lista;
        }

        public void RemoveDoctorId(int doctorId)
        {
            List<String> lines = new List<string>();
            string line;

            using (System.IO.StreamReader file = new System.IO.StreamReader(@"C:/Users/38162/Desktop/HCI/Files/doctorId.csv"))
            {
                while ((line = file.ReadLine()) != null)
                {
                    if (line.Split(';')[0] != doctorId.ToString())
                    {
                        lines.Add(line);
                    }
                }
            }

            using (System.IO.StreamWriter outfile = new System.IO.StreamWriter(@"C:/Users/38162/Desktop/HCI/Files/doctorId.csv"))
            {
                outfile.Write(String.Join(System.Environment.NewLine, lines.ToArray()));
            }
        }
    }
}
