﻿using ConsoleApp1.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace ConsoleApp1.Repository
{
    class DoctorRepository
    {
        private DoctorIdRepository doctorIdRepository;

        public DoctorRepository()
        {
            doctorIdRepository = new DoctorIdRepository();
        }

        public void AddDoctor(Model.Doctor doctor)
        {
            using (var writer = new StreamWriter(@"C:/Users/38162/Desktop/HCI/Files/doctors.csv", true))
            {
                string[] values = { doctor.Id.ToString(), doctor.FirstName, doctor.LastName, doctor.Username, doctor.Password };
                string line = String.Join(";", values);
                writer.WriteLine(line);
            }
        }


        public void RemoveDoctor(int doctorId)
        {
            doctorIdRepository.RemoveDoctorId(doctorId);

            List<String> lines = new List<string>();
            string line;

            using (System.IO.StreamReader file = new System.IO.StreamReader(@"C:/Users/38162/Desktop/HCI/Files/doctors.csv"))
            {
                while ((line = file.ReadLine()) != null)
                {
                    if (line.Split(';')[0] != doctorId.ToString())
                    {
                        lines.Add(line);
                    }
                }
            }

            using (System.IO.StreamWriter outfile = new System.IO.StreamWriter(@"C:/Users/38162/Desktop/HCI/Files/doctors.csv"))
            {
                outfile.Write(String.Join(System.Environment.NewLine, lines.ToArray()));
            }
        }


        public Model.Doctor FindDoctorById(int doctorId)
        {
            string line;
            Doctor doctor = new Doctor();
            using (System.IO.StreamReader file = new System.IO.StreamReader(@"C:/Users/38162/Desktop/HCI/Files/doctors.csv"))
            {
                while ((line = file.ReadLine()) != null)
                {
                    if (line.Split(';')[0] == doctorId.ToString())
                    {
                        doctor.Id = Int32.Parse(line.Split(';')[0]);
                        doctor.FirstName = line.Split(';')[1];
                        doctor.LastName = line.Split(';')[2];
                        doctor.Username = line.Split(';')[3];
                        doctor.Password = line.Split(';')[4];
                        break;
                    }
                }
            }
            return doctor;
        }



        public void UpdateDoctor(Doctor doctor)
        {
            List<String> lines = new List<string>();
            string line;

            using (System.IO.StreamReader file = new System.IO.StreamReader(@"C:/Users/38162/Desktop/HCI/Files/doctors.csv"))
            {
                while ((line = file.ReadLine()) != null)
                {
                    if (line.Split(';')[0] == doctor.Id.ToString())
                    {
                        //line.Split(';')[1] = medication.Name;
                        //line.Split(';')[2] = medication.Ingridients;
                        //line.Split(';')[3] = medication.Uses;
                        //lines.Add(line);

                        string[] values = { doctor.Id.ToString(), doctor.FirstName, doctor.LastName, doctor.Username, doctor.Password };
                        string line1 = String.Join(";", values);
                        lines.Add(line1);
                    }
                    else
                    {
                        lines.Add(line);
                    }
                }
            }

            using (System.IO.StreamWriter outfile = new System.IO.StreamWriter(@"C:/Users/38162/Desktop/HCI/Files/doctors.csv"))
            {
                outfile.Write(String.Join(System.Environment.NewLine, lines.ToArray()));
            }
        }

        public List<Doctor> FindAllDoctors()
        {
            string line;
            List<Doctor> lista = new List<Doctor>();

            using (System.IO.StreamReader file = new System.IO.StreamReader(@"C:/Users/38162/Desktop/HCI/Files/doctors.csv"))
            {
                while ((line = file.ReadLine()) != null)
                {
                    Doctor doctor = new Doctor();
                    doctor.Id = Int32.Parse(line.Split(';')[0]);
                    doctor.FirstName = line.Split(';')[1];
                    doctor.LastName = line.Split(';')[2];
                    doctor.Username = line.Split(';')[3];
                    doctor.Password = line.Split(';')[4];
                    lista.Add(doctor);
                }
            }

            return lista;
        }
    }
}
